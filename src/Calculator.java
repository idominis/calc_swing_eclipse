import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Window.Type;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JLabel;
import java.awt.ComponentOrientation;
import java.awt.Color;

public class Calculator {

	private JFrame frame;
	private JTextField textField;
	
	double num, ans;
	int calculation;
	
	JLabel lblNewLabel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Calculator window = new Calculator();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Calculator() {
		initialize();
	}
	
	private void aritmethic_operation() {
		switch (calculation) {
		case 1: // +
			ans = num + Double.parseDouble(textField.getText());
			textField.setText(Double.toString(ans));
			break;
		case 2: //-
			ans = num - Double.parseDouble(textField.getText());
			textField.setText(Double.toString(ans));
			break;
		case 3: // *
			ans = num * Double.parseDouble(textField.getText());
			textField.setText(Double.toString(ans));
			break;
		case 4: // /
			ans = num / Double.parseDouble(textField.getText());
			textField.setText(Double.toString(ans));
			break;

		default:
			break;
		}
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		frame = new JFrame("Calculator v1.0");
		frame.setType(Type.UTILITY);
		frame.setBounds(100, 100, 650, 450);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnNewButton = new JButton("0");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setText(textField.getText() + "0");
			}
		});
		btnNewButton.setBounds(12, 318, 117, 48);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton(".");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setText(textField.getText() + ".");			}
		});
		btnNewButton_1.setBounds(158, 318, 117, 48);
		frame.getContentPane().add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("+ / -");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String text = textField.getText();
				
				try {
					double broj = Double.parseDouble(text);
					double pozitivni;
					
					if (broj > 0) {
						textField.setText("-" + textField.getText());
					}
					else if(broj < 0){
						pozitivni = Math.abs(broj);
						textField.setText(Double.toString(pozitivni));
					}
				} catch (Exception e2) {
					textField.setText("-");
				}
				/*double broj = Double.parseDouble(text);
				double pozitivni;
				
				if (broj > 0) {
					textField.setText("-" + textField.getText());
				}
				else if (broj < 0){
					pozitivni = Math.abs(broj);
					textField.setText(Double.toString(pozitivni));
				}*/
			}
		});
		btnNewButton_2.setBounds(305, 318, 117, 48);
		frame.getContentPane().add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("1");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setText(textField.getText() + "1");
			}
		});
		btnNewButton_3.setBounds(12, 252, 117, 48);
		frame.getContentPane().add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("2");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setText(textField.getText() + "2");
			}
		});
		btnNewButton_4.setBounds(158, 252, 117, 48);
		frame.getContentPane().add(btnNewButton_4);
		
		JButton btnNewButton_5 = new JButton("3");
		btnNewButton_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setText(textField.getText() + "3");
			}
		});
		btnNewButton_5.setBounds(305, 252, 117, 48);
		frame.getContentPane().add(btnNewButton_5);
		
		JButton btnNewButton_6 = new JButton("4");
		btnNewButton_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setText(textField.getText() + "4");
			}
		});
		btnNewButton_6.setBounds(12, 186, 117, 48);
		frame.getContentPane().add(btnNewButton_6);
		
		JButton btnNewButton_7 = new JButton("5");
		btnNewButton_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setText(textField.getText() + "5");
			}
		});
		btnNewButton_7.setBounds(158, 186, 117, 48);
		frame.getContentPane().add(btnNewButton_7);
		
		JButton btnNewButton_8 = new JButton("6");
		btnNewButton_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setText(textField.getText() + "6");
			}
		});
		btnNewButton_8.setBounds(305, 186, 117, 48);
		frame.getContentPane().add(btnNewButton_8);
		
		JButton btnNewButton_9 = new JButton("7");
		btnNewButton_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setText(textField.getText() + "7");
			}
		});
		btnNewButton_9.setBounds(12, 120, 117, 48);
		frame.getContentPane().add(btnNewButton_9);

		JButton btnNewButton_10 = new JButton("8");
		btnNewButton_10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setText(textField.getText() + "8");
			}
		});
		btnNewButton_10.setBounds(158, 120, 117, 48);
		frame.getContentPane().add(btnNewButton_10);
		
		JButton btnNewButton_11 = new JButton("9");
		btnNewButton_11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setText(textField.getText() + "9");
			}
		});
		btnNewButton_11.setBounds(305, 120, 117, 48);
		frame.getContentPane().add(btnNewButton_11);
		
		JButton btnNewButton_12 = new JButton("+");
		btnNewButton_12.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//textField.setText(textField.getText() + "+");
				num = Double.parseDouble(textField.getText());
				calculation = 1;
				textField.setText("");
				lblNewLabel.setText(num + " +");
			}
		});
		btnNewButton_12.setBounds(457, 120, 58, 48);
		frame.getContentPane().add(btnNewButton_12);
		
		JButton btnNewButton_13 = new JButton("=");
		btnNewButton_13.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				aritmethic_operation();
				lblNewLabel.setText("");
			}
		});
		btnNewButton_13.setBounds(559, 252, 58, 114);
		frame.getContentPane().add(btnNewButton_13);
		
		JButton btnC = new JButton("C");
		btnC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setText("");
			}
		});
		btnC.setBounds(559, 186, 58, 48);
		frame.getContentPane().add(btnC);
		
		JButton button_1 = new JButton("-");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//textField.setText(textField.getText() + "-");
				num = Double.parseDouble(textField.getText());
				calculation = 2;
				textField.setText("");
				lblNewLabel.setText(num + " -");
			}
		});
		button_1.setBounds(457, 186, 58, 48);
		frame.getContentPane().add(button_1);
		
		JButton button_2 = new JButton("*");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//textField.setText(textField.getText() + "*");
				num = Double.parseDouble(textField.getText());
				calculation = 3;
				textField.setText("");
				lblNewLabel.setText(num + " *");
			}
		});
		button_2.setBounds(457, 252, 58, 48);
		frame.getContentPane().add(button_2);
		
		JButton button_3 = new JButton("/");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//textField.setText(textField.getText() + "/");
				num = Double.parseDouble(textField.getText());
				calculation = 4;
				textField.setText("");
				lblNewLabel.setText(num + " /");
			}
		});
		button_3.setBounds(457, 318, 58, 48);
		frame.getContentPane().add(button_3);
		
		textField = new JTextField();
		textField.setHorizontalAlignment(SwingConstants.RIGHT);
		textField.setFont(new Font("TlwgTypewriter", Font.BOLD, 37));
		textField.setBounds(294, 25, 323, 59);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton button = new JButton("B");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int length = textField.getText().length();
				int number = textField.getText().length() - 1;
				String store;
				
				if(length > 0) {
					StringBuilder back = new StringBuilder(textField.getText());
					back.deleteCharAt(number);
					store = back.toString();
					textField.setText(store);
				}
			}
		});
		button.setBounds(559, 120, 58, 48);
		frame.getContentPane().add(button);
		
		lblNewLabel = new JLabel("");
		lblNewLabel.setForeground(new Color(255, 51, 102));
		lblNewLabel.setFont(new Font("TlwgTypewriter", Font.BOLD, 30));
		lblNewLabel.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		lblNewLabel.setBounds(23, 36, 252, 42);
		frame.getContentPane().add(lblNewLabel);
	}
}
